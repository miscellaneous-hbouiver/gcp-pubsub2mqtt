FROM node:14.15-alpine3.10

WORKDIR /app

ENV GOOGLE_CLOUD_SERVICE_ACCOUNT='{"desc":"JSON service account"}' \
	GOOGLE_CLOUD_PUBSUB_SUBSCRIPTION=subscription-name \
	GOOGLE_CLOUD_PUBSUB_ACK_MESSAGE=true \
	MQTT_URL=mqtt://192.168.86.26:1883 \
	MQTT_CLIENT_NAME=gcp-pubsub-bridge \
	MQTT_TOPIC_NAME=/gcp/subscription \
	MQTT_QOS=0 \
	MQTT_RETAIN=false

COPY *.json *.js /app/
RUN npm install
CMD node pubsub.js
