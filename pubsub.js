// Copyright 2019-2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * This application demonstrates how to perform basic operations on
 * subscriptions with the Google Cloud Pub/Sub API.
 *
 * For more information, see the README.md under /pubsub and the documentation
 * at https://cloud.google.com/pubsub/docs.
 */

'use strict';
require('dotenv').config();

// unless GOOGLE_CLOUD_SERVICE_ACCOUNT, will use the default: GOOGLE_APPLICATION_CREDENTIALS
const gServiceAccount   = process.env['GOOGLE_CLOUD_SERVICE_ACCOUNT'];
const gSubscriptionName = process.env['GOOGLE_CLOUD_PUBSUB_SUBSCRIPTION'];
const gAckMessage       = process.env['GOOGLE_CLOUD_PUBSUB_ACK_MESSAGE'] === 'false' ? false : true;
const gMqttURL          = process.env['MQTT_URL']         || 'mqtt://127.0.0.1:1883';
const gMqttClientName   = process.env['MQTT_CLIENT_NAME'] || 'gcp-pubsub-bridge';
const gMqttTopicName    = process.env['MQTT_TOPIC_NAME']  || 'gcp/pubsub/subscription';  // /gcp/node-red/node-red-nest-subscription
const gMqttQOS          = process.env['MQTT_QOS'] === undefined ? 0 : +process.env['MQTT_QOS'];
const gMqttRetain       = process.env['MQTT_RETAIN'] === 'true' ? true : false;

function main() {
  const mqtt     = require('mqtt');
  const {PubSub} = require('@google-cloud/pubsub');
  const creds    = gServiceAccount ? JSON.parse(gServiceAccount) : null;

  // Creates a client; cache this for further use
  console.log(
    (new Date().toISOString()), 
    'PUBSUB: using           ', 
    creds ? 'Service account (JSON)' : 'GOOGLE_APPLICATION_CREDENTIALS'
  );
  console.log((new Date().toISOString()), 'PUBSUB: subscription    ', gSubscriptionName);
  console.log((new Date().toISOString()), 'PUBSUB: acknowledge     ', gAckMessage);
  console.log((new Date().toISOString()), 'MQTT:   URL             ', gMqttURL);
  console.log((new Date().toISOString()), 'MQTT:   Client name     ', gMqttClientName);
  console.log((new Date().toISOString()), 'MQTT:   Topic           ', gMqttTopicName);
  console.log((new Date().toISOString()), 'MQTT:   QOS             ', gMqttQOS);
  console.log((new Date().toISOString()), 'MQTT:   Retain messages ', gMqttRetain);
  
  const pubSubClient = creds ?
          new PubSub({
            projectId: creds.project_id,
            credentials: {
              client_email: creds.client_email,
              private_key:  creds.private_key,
            }
          }) :
          new PubSub();

  const mqttclient = mqtt.connect(gMqttURL, {clientId: gMqttClientName});

  const messageHandler = message => {
    const payload = {
      id: message.id,
      _name: message._name,
      publishTime: message.publishTime,
      received: message.received,
      data: JSON.parse(message.data.toString('utf8')),
      attributes: message.attributes
    };
    mqttclient.publish(
      gMqttTopicName,
      JSON.stringify(payload),
      {
        qos: gMqttQOS,
        retain: gMqttRetain
      }, function (err, result) {
        if (err) {
          console.log((new Date().toISOString()), 'MQTT:   publish ERROR=', err);
          return;
        }
        if (gAckMessage) {
          message.ack();
        }
      });

  };

  let subscription = null;
  mqttclient.on("connect",function() {
    console.log((new Date().toISOString()), 'MQTT:   connected');
    console.log((new Date().toISOString()), 'PUBSUB: subscribe ', gSubscriptionName);
    subscription = pubSubClient.subscription(gSubscriptionName);
    subscription.on('message', messageHandler);
    subscription.on('error', error => {
      console.log((new Date().toISOString()), 'PUBSUB: Received error:', error);
      process.exit(1);
    });
  });

  mqttclient.on("error",function(error){
    console.log((new Date().toISOString()), 'MQTT:   Received ERROR=', error);
    if (subscription) {
      console.log((new Date().toISOString()), 'PUBSUB: unsubscribe');
      subscription.removeListener('message', messageHandler);
    }
    process.exit(1);
  });
}

main();
